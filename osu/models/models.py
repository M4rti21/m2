from odoo import models, fields, api
from datetime import date
from dateutil.relativedelta import relativedelta

class player(models.Model):
    _name = 'osu.player'
    _description = 'Player information'

    id = fields.Integer()
    name = fields.Text()
    pp = fields.Integer()
    accuracy = fields.Float()
    account_creation = fields.Datetime(string='Account Creation', required=True)
    years = fields.Integer(compute='_compute_years', store=True)
    tournament_ids = fields.Many2many('osu.tournament', string='Tournaments')

    @api.depends('account_creation')
    def _compute_years(self):
        for player in self:
            if player.account_creation:
                avui = date.today()
                player.years = relativedelta(avui, player.account_creation).years
            else:
                player.years = 0

class tournament(models.Model):
    _name = 'osu.tournament'
    _description = 'Tournament information'

    id = fields.Integer()
    name = fields.Text()
    round_ids = fields.Many2one('osu.round', string='Rounds')
    finished = fields.Boolean()
    winner_id = fields.Many2one('osu.player', string='Winner')
    player_ids = fields.Many2many('osu.player', string='Players')

class round(models.Model):
    _name = 'osu.round'
    _description = 'Round information'

    id = fields.Integer()
    round_name = fields.Selection(string='Round name', selection=[('a', 'Round of 32'), ('b', 'Round of 16'), ('c', 'Quarter Finals'), ('d', 'Semi Finals'), ('e', 'Finals')], default='n')
    tournament_id = fields.Many2one('osu.tournament', string='Tournament')
    nm_id = fields.Many2one('osu.map', string='NM')
    hd_id = fields.Many2one('osu.map', string='HD')
    hr_id = fields.Many2one('osu.map', string='HR')
    dt_id = fields.Many2one('osu.map', string='DT')
    fm_id = fields.Many2one('osu.map', string='FM')
    tb_id = fields.Many2one('osu.map', string='TB')

class map(models.Model):
    _name = 'osu.map'
    _description = 'Map information'

    id = fields.Integer()
    name = fields.Text()
    author = fields.Text()
    mapper = fields.Text()
    difficulty = fields.Float()
    round_ids = fields.Many2one('osu.round', string='Rounds in which it appears')
